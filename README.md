This is for the case study examination for Terrabit Consulting, submitted by Ryan Bamba

This readme contains the instructions on how to run the examination and the general scope of the project.

Initial Requirements:

PHP 7.0 ^,
Composer,
Mysql server

Installation:

1. Either clone, or copy the examination files to the webserver folder (e.g. for Xampp, in htdocs)
2. Open up terminal, go to the root directory of the project and run "composer install"
3. Go to schema folder and import the api_db.sql file to you database server.
4. Edit the httpd-vhosts.conf. 
	<VirtualHost *:80>
	    DocumentRoot "path/to/project/public_folder/"
	    ServerName domainname
	</VirtualHost>
5. edit the host files. It should have "127.0.0.1 domainname" at the end
6. Restart apache2 service
7. Make sure that AllowOverride All is enabled. Or Rewrite rule is enabled for the .htaccess
8. Edit the DB settings found in /src/settings.php


NOTE: Since the instruction stated to make a REST API server, I did just that. I did not make any templates or view for the project. Checking if methods work as intended is done through POSTMAN Rest Client. x-www-form-urlencoded is used in testing the application.

Additional error filtering would've been ideal but due to time constraints, I was not able to put them in.